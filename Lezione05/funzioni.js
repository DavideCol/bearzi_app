function sum(a,b,callback) {
    var ris = a+b;
    if (typeof callback  == "function") callback(ris);
    return ris;
}

function quad(valore) {
    console.log("Il quadrato di "+valore+" è "+(valore*valore));
}

function double(valore) {
    console.log("Il doppio di "+valore+" è "+(valore*2));
}

console.log(">>",sum(5,6,double));
console.log(">>",sum(5,6,quad));
console.log(">>",sum(15,46));
console.log(">>",sum(45,64));

var stringa = "Buongiorno a tutti";
console.log(stringa.split("a"));
console.log(stringa.split(" "));
console.log(stringa.split(""));

var nume = 1209.78;
nume = nume.toString();
console.log(nume.split(""));
