class Gara {
    constructor(distanza) {
        //istanzio le variabili di istanza
        this.gara_vo = new GaraVO();
        this.gara_vo.distanza = distanza;
        this.callbackArrivato = null;
        this.callbackUpdate = null;
        this._interval_gara = null;
        this._ts_partenza = null;
        this._poolAtleti = [{
                nome: "Mario rossi",
                Sesso: "Maschio"
            },
            {
                nome: "Marino verdi",
                Sesso: "Maschio"
            },
            {
                nome: "Mariuccio neri",
                Sesso: "Maschio"
            },
            {
                nome: "Marioncino bianchi",
                Sesso: "Maschio"
            },
            {
                nome: "Marietto blu",
                Sesso: "Maschio"
            },
            {
                nome: "Marilotto topo",
                Sesso: "Maschio"
            },

            {
                nome: "Maria rossi",
                Sesso: "Femmina"
            },
            {
                nome: "Capa Tupac",
                Sesso: "Femmina"
            },
            {
                nome: "Lara Capitondi",
                Sesso: "Femmina"
            },
            {
                nome: "Loris feraaaa",
                Sesso: "Femmina"
            },
            {
                nome: "Mamma maria Nomadi",
                Sesso: "Femmina"
            },
            {
                nome: "Lucia coppola",
                Sesso: "Femmina"
            },


        ]
    }
    /**
     * crfea array atleta
     * salva in GaraVO
     * @param {Number} num_atleti
     * @returns {Atleta[]} 
     */
    generaAtleti(num_atleti) {
        var arr_atleti = [];
        //crezione callback esterno
        var callbackArrivato = () => this._arrivato();
        // creiamo coppia array
        var poolAtleti = JSON.parse(JSON.stringify(this._poolAtleti));
        console.log(poolAtleti);
        for (var i = 0; i < num_atleti; i++) {
            //calcolo posizione atleta
            var posizioneCasuale = Utis.randRange(0, poolAtleti.length - 1);
            //recupero dati
            var infoAtleta = poolAtleti.splice(posizioneCasuale, 1)[0];
            //calcolo casuale età
            var eta = Utis.randRange(15, 99);
            //creazione età
            var atleta = new Atleta(infoAtleta.nome, eta, infoAtleta.Sesso, this.gara_vo.distanza, callbackArrivato);
            arr_atleti.push(atleta);

        }
        console.log(arr_atleti);
        // aggiorno GaraVO
        this.gara_vo.atleti = arr_atleti;
        return arr_atleti;
    }

    /**
     * callback quando atleta arriva
     */
    _arrivato() {
        console.log("arrivato");
        // salvataggio atleti interno
        var atleti = this.gara_vo.atleti;
        var atletiArrivati = 0;
        for (var i = 0; i < atleti.length; i++) {
            var atleti = atleti[i];
            if (atleti.arrivato) atletiArrivati++;
        }
        //
        if (atletiArrivati >= atleti.length) {
            this._finegara();
        }



    }
    _finegara() {
        console.log("fine gara");
        clearInterval(this._interval_gara);
        this._interval_gara = null;
    }

    partenza(callbackUpdate, callbackFineGara) {
        this.callbackFineGara = callbackFineGara;
        this.callbackUpdate = callbackUpdate;
        //variabile interna corrente
        this._ts_partenza = Date.now();

        // richiamo temporized function till race end
        //todo: togliere
        this._interval_gara = setInterval(() => this._avanzamentoTempo(), 10);


    }

    _avanzamentoTempo() {
        // calcolo elapsed time
        var ts_now = Date.now();
        var ms_trascorsi = ts_now-this._ts_partenza;
        var tempoTrascorso = ms_trascorsi/1000;
        // loop tra atleti per posizione corrente
        for (var i = 0; i < this.gara_vo.atleti.length; i++) {
            var atleta = this.gara_vo.atleti[i];
            atleta.corri(tempoTrascorso);
        }
        //richiamo callback per segnalazione aggiornamento
        this.callbackUpdate();
    }
}