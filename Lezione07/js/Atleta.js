/******************************************
 *  Author : Author   
 *  Created On : Sat Nov 17 2018
 *  File : Atleta.js
 *******************************************/
class Atleta extends Persona {


    constructor(nome, eta, sesso, distanza, callbackArrivato) {
        /**
         * richiao costriuttore del padre 
         * passando parametri
         * obbligatorio
         */
        super(nome, eta, sesso);
        // assegno prop istanza

        this.velocita = this._calcolaVelocita();;
        this.callbackArrivato = callbackArrivato;
        this.distanzaPercorsa = 0;
        this.distanzaTotale = distanza;
        this.posizione = null;
        this.id = ++Atleta.tot_atleti;
        this.arrivato = false;
    }

    _calcolaVelocita() {
        var velocita = 6;
        var svantaggioEta = (this.eta - 15) / (45) * 2;
        var svantaggioSesso = (this.sesso == "Femmina") ? 0.3 : 0;
        var svantaggioCaso = Math.random()/2;
        velocita -= svantaggioEta;
        velocita -= svantaggioSesso;
        velocita -= svantaggioCaso;
        console.log(this.nome, this.eta, " - velocità: ", velocita);
        return velocita;
    }

    corri(secondi) {
        if (this.arrivato) return;
        this.distanzaPercorsa = secondi * this.velocita;
        if (this.distanzaPercorsa >= this.distanzaTotale) {
            this.arrivato = true;     
            this.callbackArrivato();
        }
        


    }
}

/**definisco statica */
Atleta.tot_atleti = 0;