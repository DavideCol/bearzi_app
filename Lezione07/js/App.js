/******************************************
 *  Author : Author   
 *  Created On : Sat Nov 17 2018
 *  File : gara.js
 *******************************************/

class App {
    constructor() {
        console.log("App");
        this.gara = null;
        this._stato = null;
        $(document).ready(() => {
            this._init();
        });
    }

    _init() {
        this.gara = new Gara(100);
        this.gara.generaAtleti(Utis.randRange(2, 8));
        // popolo pagina
        this.stampaAtleti();
        //imposto stato iniziale
        this._setStato("home");
        this._setStato("gara");
        //collego i pulsanti
        this._initPulsanti();

    }

    _initPulsanti() {
        $("#stato-home #crea").bind("click", "crea", (evento) => this._clickHandler(evento));
        $("#azioni #parti").bind("click", "parti", (evento) => this._clickHandler(evento));

    }

    /**
     * Gestisce tutti i click della applicazione
     * @param {event} evento 
     */

    _clickHandler(evento) {
        switch (evento.data) {
            case "crea":
                this._setStato("gara");
                break;
            case "parti":
                /**
                 * richiamo funzione inizio gara
                 * passaggio callback
                 */
                this.gara.partenza(() => this._aggiornaRisultato(), () => this.stampaRisultato());
                break;

            default:
                alert("click non previsto");
                break;
        }

    }

    _aggiornaRisultato() {
        // salvataggio array locale
        var atleti = this.gara.gara_vo.atleti;
        for (var i = 0; i < atleti.length; i++) {
            var atleta = atleti[i];
            // calcolo avanzamento 1 decimale
            var perc = Math.round((atleta.distanzaPercorsa / this.gara.gara_vo.distanza) * 1000) / 10;
            var div_atleta = $("#atleta_" + atleta.id);
            // recupero div barra
            var barra = div_atleta.find(".barra");
            // larghezza barra
            barra.css("width", perc + "%");

        }

    }

    /**
     * sceglie pagina da visualizzare
     * @param {string} stato 
     */
    _setStato(stato) {
        //aggiorno variabile istanza
        this._stato = stato;
        var id_div;
        //scelgo div visualizazzione pagina
        switch (stato) {
            case "home":
                id_div = "stato-home";
                break;
            case "gara":
                id_div = "stato-gara";
                break;
        }
        //nascondiamo tutti stati
        $(".stato").css("display", "none");
        //visualiziamo stato selezionato
        if (id_div) $("#" + id_div).css("display", "block");
    }
    stampaAtleti() {
        //popolare durata
        // creare div atleti
        var template = $("#template-atleta").html();
        var contenitore = $("#atleti");
        for (var i = 0; i < this.gara.gara_vo.atleti.length; i++) {
            var atleta = this.gara.gara_vo.atleti[i];
            var html = template.replace("***nome***", atleta.nome);
            html = html.replace("***eta***", atleta.eta);
            html = html.replace("***sesso***", atleta.sesso);
            html = html.replace("***id***", atleta.id);
            contenitore.append((html));
        }

    }

    stampaRisultato(gara) {

    }
}