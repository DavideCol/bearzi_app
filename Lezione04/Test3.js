var me={
    nome: "Davide",
    cognome: "Colucci",
    anni: 26,
    sesso: "maschile",
    "sport preferiti": ["Sci", "bicicletta"],
    computer: {
        marca: "windows",
        data: 2010,
    }
};
console.log(me);
console.log("Nome: "+me.nome);
console.log("Cognome: "+me["cognome"]);
console.log("Sport preferiti: "+me["sport preferiti"]);
console.log(me.computer.marca);
console.log(me);
console.log(typeof me);

if(me.anni<26){
    console.log(me.nome,me.cognome," sta imbrogliando")
} else {
    console.log(me.nome,me.cognome," Ha ragione");
};

if (me.sesso!=="maschile") console.log(me.nome,me.cognome+" non è un uomo");
if (me.sesso=="maschile") console.log(me.nome,me.cognome+" è un uomo");

if (typeof me2 !=="undefined") {
        console.log("me2 è definita",me2);
};

if (typeof me!== "undefined") {
        console.log("me è definita",me);
};

