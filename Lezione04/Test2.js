/* Le array sono dei gruppi di valori che includono un indici che riparte da 0, possono avere piu di una dimensione e possono essere innestati l'un l'altro */ 
var colori;
colori = ["rosso", "giallo", "verde",];

console.log("Pos 1:"+colori[0]);
console.log("Pos 2:"+colori[1]);
console.log("Pos 3:",colori[2]);
console.log("Pos 4:",colori[3]);
console.log("colori:"+colori);
console.log(colori+":colori");
console.log(colori[1+1]);

var colori2 = colori[0] + " chiaro";

console.log("Array completa:"+colori);
console.log(colori2);

colori[3] = "blue";
console.log("array completa dopo pus"+colori);

colori.push("marrone");
console.log("Array dopo push:",colori);

colori.push(150);
console.log("Array dopo push 2 :"+colori);

console.log("tipo valore 0:"+typeof colori[0]);
console.log("Tipo valore 6:",typeof colori[5]);
console.log("Tipo Array completa:"+typeof colori);

colori.push([1,2,3,4,5]);
console.log("array completa innestata:",colori);
console.log("valore array innestato pos 2:",colori[6][2]);
console.log("é una stringa:",(typeof colori === "string"));
console.log("è un numero:",(typeof colori[6][2] === "number"));
