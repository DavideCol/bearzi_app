/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : Test.js
 *******************************************/

 /* stampo nome e cognome */
 var nome;
 nome = "Davide";
 
 var cognome = "Colucci";
 var eta = 26;

 console.log("Esercizio java script 1");
 console.log(nome, cognome, "!!!");
 console.log(eta);
 console.log("Tra 10 anni:", (eta+10));
 console.log("Eta / 2 = "+(eta/2));
 console.log("età x 2 = "+(eta*2));
 console.log("età modulus 2 = "+(eta%2));
 console.log(eta);
 console.log("eta ++ = "+(eta++));
 console.log(eta);
 console.log("-----------");
 console.log(eta);
 console.log("eta ++ ="+(++eta));
 console.log(eta);
 console.log("Età -- ="+(--eta));
 console.log("eta -= 20 = "+(eta-=20));
 console.log("-------------------------");
 console.log("!true= "+(!true));
 console.log("true= "+(true));
 console.log("false= "+(false));
 console.log("!false= "+(!false));
 console.log("!1==2:"+(!(1==2)));
 console.log("1!=2: ",(1!=2));
 console.log("1==\"1\":",(1=="1"));
 console.log("1===\"1\":",(1==="1"));
 console.log("nome = cognome ? ",(nome==cognome));

 