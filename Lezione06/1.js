/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 14 2018
 *  File : 1.js
 *******************************************/

 function contalettere(frase) {
    // Trasformo in minuscolo 
    frase = frase.toLowerCase();
    // Trasformo in array di lettere singole
    var arr_chara = frase.split("");

    // Creo un hash dove salvaare il contatore di ogni lettera approvata
    var risultato = {};
    // stabilisco chara ammitted
    var ammessi = ("aàbcdeèfghiìjklmnoòpqrstuùvwxyz0123456789").split("");
    // faccio loop su tutte le lettere
    for (var i=0;i<arr_chara.length;i++) {
        // salvo variabile temp lettera corrente
        var lettera = arr_chara[i];
        if (ammessi.indexOf(lettera)>=0) {
        //aumento il cont lettera trovata

        if (risultato[lettera]) {
            risultato[lettera] += 1; 
        } else {
            risultato[lettera] = 1;
        }
      }
    }
    // stampo il risultato
    return risultato;
 }
 function stampaInConsole(oggetto) {
     for (var i in oggetto) {
        var contatore = oggetto[i];
        var v ="";
        if(contatore>1)  {v =["volte"];} else {v =["volta"];}
        console.log("Il carattere "+i+"che compare "+contatore+" ",+v+" nella frase")
     }
 }

 function stampaInHtml(oggetto) {
    var tempArray =[];
    for (var i in oggetto) {
       var contatore = oggetto[i];
       tempArray.push("Il carattere "+i+"che compare "+contatore+" volte nella frase");
       
    }
    document.write("<p>"+tempArray.join("</p><p>")+"</p>")
}


 //definisco la frase da controllare
 var frase= "Buongiorno, questa è la lezione numero 6 al bearzi";
 var risultato= contalettere(frase);
 console.log("----------------------------------------------");
 console.log(frase,"ha ");
stampaInConsole(risultato);
stampaInHtml(risultato);
 console.log("----------------------------------------------");