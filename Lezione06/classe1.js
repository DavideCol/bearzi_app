class Being {
    constructor() {
        console.log("A new Being has been born");
    }

    sayHi()  {
        console.log("hi, i am a being");
        this._metodoPrivato();
    }
    _metodoPrivato() {
        console.log("A private being")
    }
}

class Animal extends Being { 
    constructor(age) {
        super();
        this.age = age;
        this.favFood = "";
        this.noise = "";
        setTimeout(()=>this.makeNoise(),1000);
        console.log("New Animal");
    }

    sayHi()   {
        console.log("Hi, I am "+this.age);
    }

    run()   {
        console.log("I am running");

    }

    eat()   {
        console.log("I am eating "+this.favFood);

    }

    makeNoise() {
        console.log(this.noise);
    }
}

class Dog extends Animal {
    constructor(age) {
        super(age);
        this.favFood = "meat";
        this.noise = "Bao";
        this._noiseInterval = setInterval(()=>this.makeNoise(),500);
        console.log("I am a dog")
        console.log(this._noiseInterval);
    }
    shhht() {
        clearInterval(this._noiseInterval);
    }


}

class Cat extends Animal {
    constructor(age) {
        super(age);
        this.favFood = "fish";
        this.noise = "miao";
        console.log("I am a cat")
    }

    run() {
        super.run();
        console.log("I run like a cat");
    }

}

var b = new Being();
b.sayHi();
console.log("---------------------v--------------------------v---------------------------")
var a = new Animal(4);
console.log(a.age);
a.sayHi();
a.run();
a.eat();
//a.makeNoise();
console.log("---------------------v--------------------------v----------------------------")
var d = new Dog(prompt("Quanti anni ha il cane?"));
d.sayHi();
d.run();
d.eat();
//d.makeNoise();
console.log("---------------------v--------------------------v----------------------------")
var c = new Cat(3);
c.sayHi();
c.run();
c.eat();
//c.makeNoise();
$("h1").find("h1").css("red");